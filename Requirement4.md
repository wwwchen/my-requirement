# 各類電價表及計算範例（111 年 7 月 1 日起實施）
## 高壓電力電價
### 適用生產或非生產性質用電場所，契約容量 100 瓩以上者
#### 二段式時間電價

單位：元 

| 分類   |         |       |                             |      | 夏月(6月1日至9月30日) | 非夏月(夏月以外時間) |
|------|---------|-------|-----------------------------|------|----------------|-------------|
| 基本電費 | 經常契約    |       |                             | 每瓩每月 | 223.6          | 166.9       |
|      | 非夏月契約   |       |                             |      |                | 166.9       |
|      | 週六半尖峰契約 |       |                             |      | 44.7           | 33.3        |
|      | 離峰契約    |       |                             |      | 44.7           | 33.3        |
| 流動電費 | 週一至週五   | 尖峰時間  | 07:30 ~ 22:30               | 每度   | 4              | 3.86        |
|      |         | 離峰時間  | 00:00 ~ 07:30、22:30 ~ 24:00 |      | 1.68           | 1.56        |
|      | 週六      | 半尖峰時間 | 07:30 ~ 22:30               |      | 1.97           | 1.87        |
|      |         | 離峰時間  | 00:00 ~ 07:30、22:30 ~ 24:00 |      | 1.68           | 1.56        |
|      | 週日及離峰日  | 離峰時間  | 全日                          |      | 1.68           | 1.56        |

註：高壓供電係指以 11.4kV 或 22.8kV 供電者。

**電費計算範例** 

二段式時間電價用戶：經常契約容量 150 瓩，8 月尖峰時間用電 15,600 度、週六半尖峰時 間 5,000 度、離峰時間 13,000 度，倘最高需量未超過契約容量，當月應計電費為</br>
基本電費  223.60 元×150 瓩 ＋流動電費  4.00 元×15,600 度+1.97 元×5,000 度+1.68 元×13,000 度 ＝總電費    127,630 元

#### 三段式時間電價(尖峰時間固定)

單位：元

| 分類   |         |       |                             |                                           |      | 夏月(6月1日至9月30日) | 非夏月(夏月以外時間) |
|------|---------|-------|-----------------------------|-------------------------------------------|------|----------------|-------------|
| 基本電費 | 經常契約    |       |                             |                                           | 每瓩每月 | 223.6          | 166.9       |
|      | 半尖峰契約   |       |                             |                                           |      | 166.9          | 166.9       |
|      | 週六半尖峰契約 |       |                             |                                           |      | 44.7           | 33.3        |
|      | 離峰契約    |       |                             |                                           |      | 44.7           | 33.3        |
| 流動電費 | 週一至週五   | 尖峰時間  | 夏月                          | 10:00 ~ 12:00、13:00 ~ 17:00               | 每度   | 5.31           |             |
|      |         | 半尖峰時間 | 夏月                          | 07:30 ~ 10:00、12:00 ~ 13:00、17:00 ~ 22:30 |      | 3.54           |             |
|      |         |       | 非夏月                         | 07:30 ~ 22:30                             |      |                | 3.44        |
|      |         | 離峰時間  | 00:00 ~ 07:30、22:30 ~ 24:00 |                                           |      | 1.58           | 1.5         |
|      | 週六      | 半尖峰時間 | 07:30 ~ 22:30               |                                           |      | 1.78           | 1.71        |
|      |         | 離峰時間  | 00:00 ~ 07:30、22:30 ~ 24:00 |                                           |      | 1.58           | 1.5         |
|      | 週日及離峰日  | 離峰時間  | 全日                          |                                           |      | 1.58           | 1.5         |


**電費計算範例** 

三段式時間電價（尖峰時間固定）用戶：經常契約容量 500 瓩，8 月尖峰時間用電 10,000 度、半尖峰時間 15,000 度、週六半尖峰時間 5,000 度、離峰時間 20,000 度，倘最高需量未 超過契約容量，當月應計電費為</br>
基本電費  223.60 元×500 瓩＋流動電費  5.31 元×10,000 度+3.54 元×15,000 度+1.78 元×5,000 度                 +1.58 元×20,000 度＝總電費    258,500 元 

#### 三段式時間電價(尖峰時間可變動)

單位：元
| 分類   |         |       |                             |                                           |      | 夏月(6月1日至9月30日) | 非夏月(夏月以外時間) |
|------|---------|-------|-----------------------------|-------------------------------------------|------|----------------|-------------|
| 基本電費 | 經常契約    |       |                             |                                           | 每瓩每月 | 223.6          | 166.9       |
|      | 半尖峰契約   |       |                             |                                           |      | 166.9          | 166.9       |
|      | 週六半尖峰契約 |       |                             |                                           |      | 44.7           | 33.3        |
|      | 離峰契約    |       |                             |                                           |      | 44.7           | 33.3        |
| 流動電費 | 週一至週五   | 尖峰時間  | 夏月(指定 30 天)                 | 10:00 ~ 12:00、13:00 ~ 17:00               | 每度   | 8.67           |             |
|      |         | 半尖峰時間 | 夏月(指定 30 天)                 | 07:30 ~ 10:00、12:00 ~ 13:00、17:00 ~ 22:30 |      | 3.54           |             |
|      |         |       | 夏月(指定以外日期)                  | 07:30 ~ 22:30                             |      | 3.54           |             |
|      |         |       | 非夏月                         | 07:30 ~ 22:30                             |      |                | 3.44        |
|      |         | 離峰時間  | 00:00 ~ 07:30、22:30 ~ 24:00 |                                           |      | 1.58           | 1.5         |
|      | 週六      | 半尖峰時間 | 07:30 ~ 22:30               |                                           |      | 1.78           | 1.71        |
|      |         | 離峰時間  | 00:00 ~ 07:30、22:30 ~ 24:00 |                                           |      | 1.58           | 1.5         |
|      | 週日及離峰日  | 離峰時間  | 全日                          |                                           |      | 1.58           | 1.5         |

註：本電價之尖峰指定時間為夏月(6~9 月)，經本公司指定 30 日(180 小時)之上午 10~12 時止，下午 1~5 時止，於執行前一日下午 4 時前通知用戶。 

**電費計算範例** 

三段式時間電價（尖峰時間可變動）用戶：經常契約容量 500 瓩，8 月指定尖峰時間用電 3,000 度、半尖峰時間 22,000 度、週六半尖峰時間 5,000 度、離峰時間 20,000 度，倘最高 需量未超過契約容量，當月應計電費為</br>
基本電費  223.60 元×500 瓩 ＋流動電費  8.67 元×3,000 度+3.54 元×22,000 度+1.78 元×5,000 度                 +1.58 元×20,000 度＝總電費    256,190 元

#### 電動車充換電設施電價

※專供電動車充換電及附屬設備之用電，採高壓供電、契約容量在 100 瓩以上，且充換電設備容量超過附屬設備容量者。

單位：元 

| 分類   |           |      |     |                             |      | 夏月(6月1日至9月30日) | 非夏月(夏月以外時間) |
|------|-----------|------|-----|-----------------------------|------|----------------|-------------|
| 基本電費 | 按戶計收      |      |     |                             | 每戶每月 | 262.5          |             |
|      | 經常契約      |      |     |                             | 每瓩每月 | 47.2           | 34.6        |
| 流動電費 | 週一至週五     | 尖峰時間 | 夏月  | 16:00 ~ 22:00               | 每度   | 8.35           |             |
|      |           |      | 非夏月 | 15:00 ~ 21:00               |      |                | 8.13        |
|      |           | 離峰時間 | 夏月  | 00:00 ~ 16:00、22:00 ~ 24:00 |      | 2.05           |             |
|      |           |      | 非夏月 | 00:00 ~ 15:00、21:00 ~ 24:00 |      |                | 1.95        |
|      | 週六、週日及離峰日 | 離峰時間 |     | 全日                          |      | 2.05           | 1.95        |

註：以高壓供電者，按本表單價 95%計算。

**電費計算範例**

電動車充換電設施電價用戶：經常契約容量 280 瓩，避開尖峰時間用電，8 月尖峰時間用電 0 度、離峰時間 25,000 度，倘最高需量未超過契約容量，當月應計電費為</br>
基本電費  262.50 元+47.20 元×280 瓩 ＋流動電費  2.05 元×25,000 度 ＝總電費    64,729 元×95%=61,493 
